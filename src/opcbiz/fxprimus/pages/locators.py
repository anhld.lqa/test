class Login:
    username_id = ''
    password_id = ''
    login_id = ''


class Register:
    account_type_id = 'mat-select-0'
    account_individual_xpath = '//*[@id="mat-option-0"]//span[contains(text(), "Individual")]'
    account_joint_xpath = '//*[@id="mat-option-1"]//span[contains(text(), "Joint")]'
    account_corporate_xpath = '//*[@id="mat-option-2"]//span[contains(text(), "Corporate")]'
    first_name_id = 'firstName'
    last_name_id = 'lastName'
    email_id = 'email'
    country_id = 'mat-select-1'
    country_search_field_xpath = "//*[@placeholder='Search Country ']"
    phone_number_id = 'phoneNumber'
    password_id = 'password'
    checkbox_agreement_id = 'clientAgreement'
    checkbox_kid_id = 'kid_policy'
    checkbox_marketing_id = 'newsletter'
    checkbox_understand_id = 'hungary_policy'

    register_now_xpath = "//form/button"

    continue_register_xpath = "//span[contains(text(),'CONTINUE')]"
