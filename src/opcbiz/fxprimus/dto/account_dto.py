from collections import namedtuple
from src.opcbiz.fxprimus.utils.excel_utils import ExcelFieldDto


class AccountDto:
    def __init__(self, account_id, username, password, account_type):
        self.account_id = account_id
        self.username = username
        self.password = password
        self.account_type = account_type

    # getter method
    def get_account_id(self):
        return self.account_id

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_account_type(self):
        return self.account_type

    # setter method
    def set_account_id(self, account_id):
        self.account_id = account_id

    def set_username(self, username):
        self.username = username

    def set_password(self, password):
        self.password = password

    def set_account_type(self, account_type):
        self.account_type = account_type

    def custom_dto_decoder(self):
        return namedtuple('AccountDto', self.keys())(*self.values())

    @staticmethod
    def excel_template():
        excel_field_dto_list = [ExcelFieldDto('ACCOUNT_ID', 'account_id'),
                                ExcelFieldDto('USERNAME', 'username'),
                                ExcelFieldDto('PASSWORD', 'password'),
                                ExcelFieldDto('ACCOUNT_TYPE', 'account_type')]
        return excel_field_dto_list
