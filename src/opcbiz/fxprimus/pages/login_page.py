from src.opcbiz.fxprimus.pages.locators import Login
from src.opcbiz.fxprimus.pages.base_page import BasePage


class LoginPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.username_elt = self.find_element_by_id(Login.username_id)
        self.password_elt = self.find_element_by_id(Login.password_id)
        self.login_elt = self.find_element_by_id(Login.login_id)
