class Constant:
    # Web Drivers
    BROWSER_DRIVER_CHROME_WIN = r"webdriver/chromedriver.exe"
    BROWSER_DRIVER_FIREFOX_WIN = r"webdriver/geckodriver.exe"
    BROWSER_DRIVER_EDGE_WIN = r"webdriver/msedgedriver.exe"
    BROWSER_DRIVER_CHROME_MAC = r"webdriver/chromedrivermac"

    # Browsers
    CHROME = "Chrome"
    FIREFOX = "Firefox"
    EDGE = "Edge"
    CHROME_MAC = "Chrome Mac"
    SAFARI_MAC = "Safari"

    #Gmail
    GMAIL_USERNAME = 'toannguyenopcbiz@gmail.com'
    GMAIL_PASSWORD = 'Lqa@12345'

    # Test123!
    PASSWORD_TEST123 = 'Test123!'

    # Except and else case
    CAN_NOT_LOGIN = 'Can not login'
