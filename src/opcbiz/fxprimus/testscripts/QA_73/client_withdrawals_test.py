import csv
import json
import threading
import time
import requests
import unittest
from datetime import datetime

from requests.exceptions import ConnectionError

from src.opcbiz.fxprimus.constant.constant import Constant
from src.opcbiz.fxprimus.constant.url_constant import UrlConstant
import src.opcbiz.fxprimus.testscripts.QA_73.get_data as get_data
from src.opcbiz.fxprimus.testscripts.base_test import BaseTest as bt


class ClientWithdrawalsTest(bt, unittest.TestCase):
    clients_dto: list
    current_time: str

    def setUp(self):
        print('setUp')
        global clients_dto
        global current_time

        clients_dto = get_data.get_clients_list_from_csv('QA_73_Clients.csv')

        now = datetime.now()
        current_time = now.strftime("%H_%M_%S_")

    def get_wd_request_status(self, json_body):
        try:
            json_body = json.loads(json_body)
            data = json_body['data']
            wd_request_status = data['status']
        except:
            wd_request_status = 'except: can not extract wd request status'
        return wd_request_status

    def extract_error_message(self, response_text):
        try:
            json_body = json.loads(response_text)
            error = str(json_body['errors'])
        except:
            error = 'except: can not extract error message'
        return error

    def row_for_writerow(self, num_case, client_dto, note):
        print(str(num_case) + ': ' + note)
        row_for_writerow = [str(num_case), client_dto.get_client_id(), '_',
                            client_dto.get_other_credentials(), '_', '_', note]
        return row_for_writerow

    def call_api_and_extract_result(self, ranger_case):

        # ---------- set limit range ----------#
        start_case = ranger_case[0]
        end_case = ranger_case[1]

        # ---------- Create report file ----------#
        report_name = 'Client_Withdrawal_13012021' + '_' + current_time + str(start_case) + '_' + str(end_case) + ".csv"
        file_path = bt.create_file_path_result(report_name)

        result = open(file_path, mode='w', newline='')
        results_writer = csv.writer(result, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        result_header = ['no', 'client_id', 'mt4_id', 'username', 'Had Withdrawal in the past',
                         'Withdrawal Request Status', 'Failed Reason']
        results_writer.writerow(result_header)

        for num_case, client_dto in enumerate(clients_dto):
            print(num_case)
            # ---------- limit the instances that will run ----------#
            if num_case not in range(start_case, end_case):
                if num_case < start_case:
                    continue
                else:
                    break

            # ---------- IGNORE username is null ----------#
            if str(client_dto.get_other_credentials()) == 'null':
                note = 'IGNORE'
                results_writer.writerow(self.row_for_writerow(num_case, client_dto, note))
                continue

            # ---------- Login and get token ----------#
            token = bt.get_client_token(client_dto.get_other_credentials(), Constant.PASSWORD_TEST123)

            if bt.logged(token):  # if logged

                # ---------- Call call mt4 account & get response ----------#
                headers_bearer_auth = bt.create_headers_bearer_auth(token)
                mt4_rps = requests.get(UrlConstant.END_POINT_MT4_ACCOUNTS, headers=headers_bearer_auth)

                mt4_rps_stt_code = str(mt4_rps.status_code)
                mt4_rps_text = mt4_rps.text

                if mt4_rps_stt_code.startswith('2'):  # if successful
                    json_body = json.loads(mt4_rps_text)
                    data = json_body['data']
                    if len(data) > 0:  # if client has mt4 account
                        have_mt4_live = False
                        for mt4_index, mt4 in enumerate(data):
                            if mt4['status'] and not mt4['virtual']:
                                have_mt4_live = True
                                payload = {
                                    "mt4_id": mt4['mt4_id'],
                                    "base_amount": "25"
                                }
                                # ---------- Call withdrawal request - create  & get response ----------#
                                try:
                                    wd_rps = requests.post(UrlConstant.END_POINT_WITHDRAWAL_CREATE, data=payload,
                                                           headers=headers_bearer_auth)
                                    wd_stt_code = str(wd_rps.status_code)
                                    wd_rps_text = wd_rps.text

                                    if wd_stt_code.startswith('2'):  # if successful
                                        # ---------- Writerow in report file ----------#
                                        row_for_writerow = [str(num_case) + '-' + str(mt4_index),
                                                            client_dto.get_client_id(), mt4['mt4_id'],
                                                            client_dto.get_other_credentials(), '_',
                                                            self.get_wd_request_status(wd_rps_text), '_']
                                        print(row_for_writerow)
                                        results_writer.writerow(row_for_writerow)
                                    else:
                                        error_message = 'Code ' + wd_stt_code + ' ' + self.extract_error_message(
                                            wd_rps_text)

                                        row_for_writerow = [str(num_case) + '-' + str(mt4_index),
                                                            client_dto.get_client_id(), mt4['mt4_id'],
                                                            client_dto.get_other_credentials(), '_', '_', error_message]
                                        print(row_for_writerow)
                                        results_writer.writerow(row_for_writerow)
                                except ConnectionError as ex:
                                    print(ex)


                        if not have_mt4_live:
                            error_message = 'Client Mt4 live Account did not found in the staging mt4 server'
                            row_for_writerow = [str(num_case), client_dto.get_client_id(), '_',
                                                client_dto.get_other_credentials(), '_', '_', error_message]
                            results_writer.writerow(row_for_writerow)
                    else:
                        error_message = 'Client Mt4 Account did not found in the staging mt4 server'
                        row_for_writerow = [str(num_case), client_dto.get_client_id(), '_',
                                            client_dto.get_other_credentials(), '_', '_', error_message]
                        results_writer.writerow(row_for_writerow)
                else:
                    error_message = 'Code ' + mt4_rps_stt_code + ': Can not get mt4 account'
                    row_for_writerow = [str(num_case), client_dto.get_client_id(), '_',
                                        client_dto.get_other_credentials(), '_', '_', error_message]
                    results_writer.writerow(row_for_writerow)
            else:
                error_message = token
                row_for_writerow = [str(num_case), client_dto.get_client_id(), '_',
                                    client_dto.get_other_credentials(), '_', '_', error_message]
                results_writer.writerow(row_for_writerow)
        result.close()

    def test_client_withdrawals(self, ):
        print('test_client_withdrawals')
        try:
            t = time.time()

            # ---------------------------------------------- #

            arr1 = [0, 5]
            arr2 = [5, 10]

            print('threading')
            th1 = threading.Thread(target=self.call_api_and_extract_result, args=(arr1,))
            th2 = threading.Thread(target=self.call_api_and_extract_result, args=(arr2,))

            print('start')
            th1.start()
            th2.start()

            print('join')
            th1.join()
            th2.join()

            # ---------------------------------------------- #
            # arr1 = [0, 250]
            # arr2 = [250, 500]
            # arr3 = [500, 750]
            # arr4 = [750, 1000]
            # arr5 = [1000, 1250]
            # arr6 = [1250, 1500]
            # arr7 = [1500, 1750]
            # arr8 = [1750, 2000]
            #
            # print('threading')
            # th1 = threading.Thread(target=self.call_api_and_extract_result, args=(arr1,))
            # th2 = threading.Thread(target=self.call_api_and_extract_result, args=(arr2,))
            # th3 = threading.Thread(target=self.call_api_and_extract_result, args=(arr3,))
            # th4 = threading.Thread(target=self.call_api_and_extract_result, args=(arr4,))
            # th5 = threading.Thread(target=self.call_api_and_extract_result, args=(arr5,))
            # th6 = threading.Thread(target=self.call_api_and_extract_result, args=(arr6,))
            # th7 = threading.Thread(target=self.call_api_and_extract_result, args=(arr7,))
            # th8 = threading.Thread(target=self.call_api_and_extract_result, args=(arr8,))
            #
            # print('start')
            # th1.start()
            # th2.start()
            # th3.start()
            # th4.start()
            # th5.start()
            # th6.start()
            # th7.start()
            # th8.start()
            #
            # print('join')
            # th1.join()
            # th2.join()
            # th3.join()
            # th4.join()
            # th5.join()
            # th6.join()
            # th7.join()
            # th8.join()

            print("done in ", time.time() - t)
        except:
            print("error")

    def tearDown(self):
        print('tearDown')


if __name__ == '__main__':
    unittest.main()
