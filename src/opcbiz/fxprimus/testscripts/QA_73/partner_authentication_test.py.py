import csv
from datetime import datetime
import json
import unittest

import requests

from src.opcbiz.fxprimus.testscripts.base_test import BaseTest
from src.opcbiz.fxprimus.constant.url_constant import UrlConstant
import src.opcbiz.fxprimus.testscripts.QA_73.get_data as get_data


class CanLoginTest(unittest.TestCase, BaseTest):
    partners_dto: list
    start_case: int
    end_case: int
    current_time: str

    def setUp(self):
        print('setUp')

        global partners_dto
        global start_case
        global end_case
        global current_time

        partners_dto = get_data.get_partners_list_from_csv('QA_73_Partners.csv')

        start_case = 25000
        end_case = 31000

        now = datetime.now()
        current_time = now.strftime("%H_%M_%S_")

    def extract_token(self, response_text):
        json_body = json.loads(response_text)
        data = json_body['data']
        return data

    def extract_error_message(self, response_text):
        json_body = json.loads(response_text)
        error = json_body['errors']
        return error

    def get_type(self, token):
        bearer_auth = "Bearer " + token
        headers = {"Authorization": bearer_auth}
        response = requests.get(UrlConstant.END_POINT_PARTNERS, headers=headers)
        if response.status_code == 200:
            json_body = json.loads(response.text)
            data = json_body['data']
            partners_type = data['partner_type']
            return partners_type
        else:
            return 'Can not get type'

    def test_can_login_partner(self):
        account_type: str
        pass_fail: str
        error_message: str

        report_name = 'QA_73_Partner' + '_' + current_time + str(start_case) + '_' + str(end_case) + ".csv"
        file_path = BaseTest.create_file_path_result(report_name)

        result = open(file_path, mode='w', newline='')
        results_writer = csv.writer(result, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        result_header = ['No', 'Username', 'Partner_id', 'Partner_Type', 'Can Login',
                         'Error Message']
        results_writer.writerow(result_header)

        for num_case, partner in enumerate(partners_dto):
            if num_case not in range(start_case, end_case):
                if num_case < start_case:
                    continue
                else:
                    break

            if str(partner.get_username()) == 'null':
                account_type = ''
                pass_fail = 'IGNORE'
                error_message = ''
            else:
                payload_login = {"username": partner.get_username(),
                                 "password": 'Test123!'}
                response = requests.post(UrlConstant.END_POINT_LOGIN_PARTNER, data=payload_login)
                status_request = response.status_code
                print(str(num_case) + ': ' + str(status_request))

                if status_request == 200:
                    token = self.extract_token(response.text)
                    account_type = self.get_type(token)
                    pass_fail = 'PASS'
                    error_message = ''
                else:
                    account_type = ''
                    pass_fail = 'FAIL'
                    error_message = self.extract_error_message(response.text)

            row_for_writerow = [str(num_case),
                                partner.get_username(),
                                partner.get_partner_id(),
                                account_type,
                                pass_fail,
                                error_message]
            results_writer.writerow(row_for_writerow)
        result.close()
        print('test_can_login_partner')

    def tearDown(self):
        print('tearDown')


if __name__ == '__main__':
    unittest.main()
