from src.opcbiz.fxprimus.dto import qa_73_dto
from src.opcbiz.fxprimus.testscripts.base_test import BaseTest as bt

import src.opcbiz.fxprimus.utils.excel_utils as eut


# get all data from CLIENTS csv
# return a list client dto
def get_clients_list_from_csv(file_name):
    clients_in_sheet = eut.get_data_from_csv(bt.create_file_path_input(file_name),
                                             qa_73_dto.ClientDto.excel_template(),
                                             qa_73_dto.ClientDto)
    clients_dto = []
    for client in clients_in_sheet:
        if str(client.other_credentials).__contains__(','):
            credentials = str(client.other_credentials).split(',')
            # for credential in credentials:
            clients_dto.append(qa_73_dto.ClientDto(client.id, client.client_id, client.profile_status, credentials[0]))
        else:
            clients_dto.append(
                qa_73_dto.ClientDto(client.id, client.client_id, client.profile_status, client.other_credentials))
    return clients_dto


# get all data from PARTNERS csv
# return a list client dto
def get_partners_list_from_csv(file_name):
    partners_in_sheet = eut.get_data_from_csv(bt.create_file_path_input(file_name),
                                              qa_73_dto.PartnerDto.excel_template(),
                                              qa_73_dto.PartnerDto)
    partners_dto = []
    for partner in partners_in_sheet:
        partners_dto.append(qa_73_dto.PartnerDto(partner.profile_status, partner.username, partner.partner_id))
    return partners_dto
