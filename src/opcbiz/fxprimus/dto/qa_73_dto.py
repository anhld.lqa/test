from collections import namedtuple
from src.opcbiz.fxprimus.utils.excel_utils import ExcelFieldDto


class ClientDto:
    def __init__(self, id, client_id, profile_status, other_credentials):
        self.id = id
        self.client_id = client_id
        self.profile_status = profile_status
        self.other_credentials = other_credentials

    # getter method
    def get_client_id(self):
        return self.client_id

    def get_id(self):
        return self.id

    def get_profile_status(self):
        return self.profile_status

    def get_other_credentials(self):
        return self.other_credentials

    # setter method
    def set_client_id(self, client_id):
        self.client_id = client_id

    def set_id(self, id):
        self.id = id

    def set_profile_status(self, profile_status):
        self.profile_status = profile_status

    def set_other_credentials(self, other_credentials):
        self.other_credentials = other_credentials

    def custom_dto_decoder(self):
        return namedtuple('ClientDto', self.keys())(*self.values())

    @staticmethod
    def excel_template():
        excel_field_dto_list = [ExcelFieldDto('_id', 'id'),
                                ExcelFieldDto('client_id', 'client_id'),
                                ExcelFieldDto('profile_status', 'profile_status'),
                                ExcelFieldDto('other_credentials', 'other_credentials')]
        return excel_field_dto_list


class PartnerDto:
    def __init__(self, profile_status, username, partner_id):
        self.profile_status = profile_status
        self.username = username
        self.partner_id = partner_id

    # getter method
    def get_profile_status(self):
        return self.profile_status

    def get_username(self):
        return self.username

    def get_partner_id(self):
        return self.partner_id

    # setter method
    def set_profile_status(self, profile_status):
        self.profile_status = profile_status

    def set_username(self, username):
        self.username = username

    def set_partner_id(self, partner_id):
        self.partner_id = partner_id

    def custom_dto_decoder(self):
        return namedtuple('PartnerDto', self.keys())(*self.values())

    @staticmethod
    def excel_template():
        excel_field_dto_list = [ExcelFieldDto('profile_status', 'profile_status'),
                                ExcelFieldDto('username', 'username'),

                                ExcelFieldDto('partner_id', 'partner_id')]
        return excel_field_dto_list


class WithdrawalsDto:
    def __init__(self, _id, mt4_id, client_id, username, account_type, balance, currency, status, note):
        self._id = _id
        self.mt4_id = mt4_id
        self.client_id = client_id
        self.username = username
        self.account_type = account_type
        self.balance = balance
        self.currency = currency
        self.status = status
        self.note = note
