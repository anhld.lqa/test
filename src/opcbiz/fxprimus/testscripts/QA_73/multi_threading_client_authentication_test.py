import csv
from datetime import datetime
import json
import unittest
import requests
import threading
import time

from requests.exceptions import ConnectionError

from src.opcbiz.fxprimus.testscripts.base_test import BaseTest
from src.opcbiz.fxprimus.constant.url_constant import UrlConstant
import src.opcbiz.fxprimus.testscripts.QA_73.get_data as get_data


class CanLoginTest(unittest.TestCase, BaseTest):
    clients_dto: list

    def setUp(self):
        print('setUp')

        global clients_dto
        clients_dto = get_data.get_clients_list_from_csv('QA_73_Clients.csv')
        print(len(clients_dto))

    def extract_token(self, response_text):
        try:
            json_body = json.loads(response_text)
            data = json_body['data']
        except:
            data = 'except: can not extract token'
        return data

    def extract_error_message(self, response_text):
        try:
            json_body = json.loads(response_text)
            error = json_body['errors']
        except:
            error = 'except: can not extract error message'
        return error

    def get_type(self, token):
        bearer_auth = "Bearer " + token
        headers = {"Authorization": bearer_auth}
        try:
            self.wait(1)
            response = requests.get(UrlConstant.END_POINT_CLIENTS, headers=headers)
        except Exception as ex:
            print(ex)
            return ex
        if str(response.status_code).startswith('2'):
            json_body = json.loads(response.text)
            data = json_body['data']
            client_type = data['client_type']
            return client_type
        else:
            mess = 'Status code:' + str(response.status_code) + ' - Can not get type'
            return mess

    def call_api_and_extract_result(self, ranger_case):

        start_case = ranger_case[0]
        end_case = ranger_case[1]

        report_name = 'Client_Authentication_13012021_40k' + '_' + self.get_current_time() + str(
            start_case) + '_' + str(
            end_case) + ".csv"
        file_path = BaseTest.create_file_path_result(report_name)

        result = open(file_path, mode='w', newline='')
        results_writer = csv.writer(result, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        result_header = ['no', 'username', 'client_id', 'client_type', 'can login', 'status code', 'error message']
        results_writer.writerow(result_header)

        for num_case, client in enumerate(clients_dto):
            username = str(client.get_other_credentials())
            if num_case not in range(start_case, end_case):
                if num_case < start_case:
                    continue
                else:
                    break
            account_type = ''
            pass_fail = ''
            error_message = ''
            status_request = 0

            if username == 'null' or username == '':
                account_type = ''
                pass_fail = 'IGNORE'
                error_message = ''
            else:
                payload_login = {"username": username,
                                 "password": 'Test123!'}
                try:
                    self.wait(1)
                    response = requests.post(UrlConstant.END_POINT_LOGIN_CLIENT, data=payload_login)
                    status_request = str(response.status_code)
                    print(str(num_case) + ': ' + status_request)

                    if status_request.startswith('2'):
                        token = self.extract_token(response.text)
                        account_type = self.get_type(token)
                        pass_fail = 'PASS'
                        error_message = ''
                    else:
                        account_type = ''
                        pass_fail = 'FAIL'
                        error_message = self.extract_error_message(response.text)
                    if status_request.startswith('5'):
                        self.wait(3)
                except Exception as ex:
                    print(ex)
                    error_message = ex

            row_for_writerow = [str(num_case), username, client.get_client_id(), account_type, pass_fail,
                                status_request, error_message]
            results_writer.writerow(row_for_writerow)
        result.close()
        print('test_can_login')

    def test_can_login_client(self):
        try:
            t = time.time()

            arr1 = [0, 1000]
            arr2 = [1000, 2000]
            arr3 = [2000, 3000]
            arr4 = [3000, 4000]
            arr5 = [4000, 5000]

            # arr6 = [25000, 30000]
            # arr7 = [30000, 35000]
            # arr8 = [35000, 40000]
            # arr9 = [40000, 45000]
            # arr10 = [45000, 50000]

            print('threading')
            th1 = threading.Thread(target=self.call_api_and_extract_result, args=(arr1,))
            th2 = threading.Thread(target=self.call_api_and_extract_result, args=(arr2,))
            th3 = threading.Thread(target=self.call_api_and_extract_result, args=(arr3,))
            th4 = threading.Thread(target=self.call_api_and_extract_result, args=(arr4,))
            th5 = threading.Thread(target=self.call_api_and_extract_result, args=(arr5,))

            # th6 = threading.Thread(target=self.call_api_and_extract_result, args=(arr6,))
            # th7 = threading.Thread(target=self.call_api_and_extract_result, args=(arr7,))
            # th8 = threading.Thread(target=self.call_api_and_extract_result, args=(arr8,))
            # th9 = threading.Thread(target=self.call_api_and_extract_result, args=(arr9,))
            # th10 = threading.Thread(target=self.call_api_and_extract_result, args=(arr10,))

            print('start')
            th1.start()
            th2.start()
            th3.start()
            th4.start()
            th5.start()

            # th6.start()
            # th7.start()
            # th8.start()
            # th9.start()
            # th10.start()

            print('join')
            th1.join()
            th2.join()
            th3.join()
            th4.join()
            th5.join()

            # th6.join()
            # th7.join()
            # th8.join()
            # th9.join()
            # th10.join()

            print("done in ", time.time() - t)
        except:
            print("error threading")

        print('test_can_login')

    def tearDown(self):
        print('tearDown')


if __name__ == '__main__':
    unittest.main()
